# DevOps Project 6


## README
Hello this is my sixth project for the **DevOps Bootcamp** so every project is different, and the project will have different sections that I will need to apply. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed.

## Project Name
**Website Monitoring and Recovery.**

---

## Description
1. _Create a server on a cloud platform._
2. _Install Docker and run a Docker container on the remote server._
3. _Write a Python script that send an email notification when the website is down._
4. _Write a Python script that automatically restart the application & server when the application in down._


## Technologies to use
***Python, Linode, Docker, Linux.***

## What did I build? 
- Created 3 python scripts for automation.
- Created a Docker Image to create a web container.
- Created a Htlm file to add it as a webpage.

## What I have learned?
I have learned to automate task with python, learned how to create a docker file to build a image, learned how to send an email notification with python, learned how to automatically restart the web service with python and the Linux system.

## Please describe the project
Need to describe this project.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
